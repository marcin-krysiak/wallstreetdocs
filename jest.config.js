module.exports = {
  // verbose: true,
  testPathIgnorePatterns: ["Gruntfile.js"],
  collectCoverageFrom: ["src/**/*.js", "server.js"]
}
