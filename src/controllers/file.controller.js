var stream = require("stream");
var db = require("../database.js");
var File = db.files;

exports.uploadFile = function(req, res) {
  var promises = [];
  var fileNames = [];

  if (!req.files || req.files.length === 0) res.send(400, 'no file attached');
  else {
    for (var i = 0, len = req.files.length; i < len; i++) {
      var file = req.files[i];
      fileNames.push(file.originalname);
      promises.push(
        File.create({
          type: file.mimetype,
          name: file.originalname,
          data: file.buffer
        })
      );
    }

    Promise.all(promises)
      .then(function() {
        res.json({ msg: "Files uploaded successfully!", fileNames: fileNames });
      })
      .catch(function(err) {
        console.log(err);
        res.json({ msg: "Error", detail: err });
      });
  }
};

exports.listAllFiles = function(req, res) {
  File.findAll({ attributes: ["id", "name"] })
    .then(function(files) {
      res.json(files);
    })
    .catch(function(err) {
      console.log(err);
      res.json({ msg: "Error", detail: err });
    });
};

exports.downloadFile = function(req, res) {
  File.findById(req.params.id)
    .then(function(file) {
      var fileContents = Buffer.from(file.data, "base64");
      var readStream = new stream.PassThrough();
      readStream.end(fileContents);

      res.set("Content-disposition", "attachment; filename=" + file.name);
      res.set("Content-Type", file.type);

      readStream.pipe(res);
    })
    .catch(function(err) {
      console.log(err);
      res.json({ msg: "Error", detail: err });
    });
};
