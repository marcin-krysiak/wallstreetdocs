var multer = require("multer");
var fileWorker = require("../controllers/file.controller.js");

var storage = multer.memoryStorage();
var upload = multer({ storage: storage });

module.exports = function(app) {
  app
    .route("/api/file/upload")
    .post(upload.array("file", 5), fileWorker.uploadFile);
  app.route("/api/file/info").get(fileWorker.listAllFiles);
  app.route("/api/file/:id").get(fileWorker.downloadFile);
};
