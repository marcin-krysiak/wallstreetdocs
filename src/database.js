var Sequelize = require("sequelize");

var dbConfig = {
  database: "db",
  username: "elf",
  password: "",
  host: "localhost",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};

var sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.username,
  dbConfig.password,
  {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    operatorsAliases: false,
    // logging: false,

    pool: {
      max: dbConfig.max,
      min: dbConfig.pool.min,
      acquire: dbConfig.pool.acquire,
      idle: dbConfig.pool.idle
    }
  }
);

var db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.files = require("./models/file.model.js")(sequelize, Sequelize);

module.exports = db;
