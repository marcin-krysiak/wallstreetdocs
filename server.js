var express = require("express");
var exphbs = require("express-handlebars");
var morgan = require('morgan')
var db = require("./src/database");

var app = express();
app.use(morgan('tiny'));

app.engine("handlebars", exphbs({
  defaultLayout: "main"
}));
app.set("view engine", "handlebars");

app.use("/scripts", express.static(__dirname + '/node_modules/'));
app.use("/styles", express.static(__dirname + '/views/styles/'));

app.get("/", function(req, res) {
  res.render("home");
});

var normalizedPath = require("path").join(__dirname, "src/routes");
require("fs")
  .readdirSync(normalizedPath)
  .forEach(function(file) {
    if (file.indexOf('.test.') === -1) require("./src/routes/" + file)(app);
  });

var port = process.env.PORT || 3000;
app.set("port", port);

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.info("Listening on port", port);
  }
});

module.exports = app;
