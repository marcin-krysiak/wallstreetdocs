"use strict";

module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-run');

  grunt.initConfig({
    run: {
      jest: {
        cmd: 'jest',
        args: [
          '--coverage'
        ]
      }
    }
  });

  grunt.registerTask("test", ["run:jest"]);
};
